from setuptools import setup, find_packages
from setuptools.extension import Extension
from setuptools import dist

setup(
    name='experiment_bags',
    # version='0.1',
    author='Matt Estrada',
    author_email='matthew.estrada@epfl.ch',
    description='LFS directory for ROS experiment bag files',
    packages=find_packages(),  # Required
)

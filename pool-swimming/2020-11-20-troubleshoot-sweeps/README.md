# Troubleshoot sweeps 

Matt and Astha troubleshooting recording batches of bags for swimming sweeps.

Data is not publishable but meant to help troubleshoot the parsing code. USB
camera was missing from these experiments due to an error in `usb_cam` node.

Experiment varied **omega**, the frequency of the wave propagated along the
body. Frequency is in [hz]. This data shouldnt be needed since we dumped the
`rosparams` into a `yaml` by the same name of the corresponding bag file.

## Experiment Notes
     
|---------------------------------|-----------|-------------------------------------------------|
| **bag name**: astha_matt_sweep- | **omega** | **notes**                                       |
|---------------------------------|-----------|-------------------------------------------------|
| 2020-11-20-14-43-37.bag         |       0.6 | first paramters half lights on                  |
| 2020-11-20-14-55-18.bag         |       0.6 | same parameters all lights off                  |
| 2020-11-20-14-57-30.bag         |       0.7 | all lights stayed off for rest of experiments   |
| 2020-11-20-15-00-19.bag         |       0.8 |                                                 |
| 2020-11-20-15-02-46.bag         |       0.9 | started rosbag after krock entered initialstate |
| 2020-11-20-15-05-04.bag         |       0.5 |                                                 |
|---------------------------------|-----------|-------------------------------------------------|

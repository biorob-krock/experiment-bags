# Pool Experiments

## Procedure 

Krock swimming experiments with pool tracking. A fiducial of green LEDs is
mounted on krock's front girdle to measure planar position and orientation.

Useful ROS topics are coming from the robot and the pool tracking computer over
the network:

    /krock2/jointMeasuredData
    /krock2/jointCommand
    /krock2/imuRPY
    /pool_track/pt_cloud
    /pool_track/pt_count

Two cameras are needed to cover the full length of the pool, which needs to be
accounted for when analyzing the data.

Pool calibration procedure is described on the [biorob wiki](https://biorob.epfl.ch/wiki/documentation:pool_camera#calibration).


## Scripts

#### Recording


[pool_experiment.sh](https://gitlab.com/biorob-krock/krock-pckgs/parse/pool_experiment.sh)
coordinates the `water_experiment.launch` file to record camera, pool point
tracking, and robot information. This should save a `.bag` along with `.yml` of
the same name, dumping the ros parameters coordinating swimming.

#### Parsing
From the `experiment-parsing` parent directory, parse an example bag file containing all the topics listed above:

    python3 swim_parse.py

Original bag files are stored in
`biorobnfs.epfl.ch/projects/NCCR/bagfiles/pool-experiments`. Unfiltered bag
files contain the video footage of experiments and can be replayed with 
`review_pool_experiment.sh` found in the [krock_pckgs](https://gitlab.com/biorob-krock/krock-pckgs) repository.

#### Extracting video

[rosbag2video](https://github.com/mlaiacker/rosbag2video) worked well. Videos were captured and played bag using `Noetic` and run on `Ubuntu 20.04` running `Python 3.8.5`. Did not need to make a virtualenv.

Two, identical `.mp4` files were generated, duplicated from `image_view` and `image_raw` topics publishing the same data. 


## TODOs

- deal with erroneous points in point tracking
- do experiments with proper `.yml` parameter dump
- standardize velocity calculation for swimming performance sweeps


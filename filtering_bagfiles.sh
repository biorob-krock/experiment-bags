#!/bin/bash

# Aim: The script reads the bag files in the current diretory and generates 2 new bags 
# 	-> original data, w/o camera data
# 	-> original data, only camera data
# Run: run the script from the directory that has a ros bag 
#    $ cd path/to/bagfiles 
# 	 $ source ./path/to/script/filtering_bagfiles.sh

# ToDo:
# 	1. Create a function for filtering and compress based on topic list

# Color of the output
C_RED='\033[0;31m'
C_GREEN='\033[0;32m'
C_YELLOW='\033[0;33m'
C_ORANGE='\033[1;33m'
C_BLUE='\033[1;34m'
C_PURPLE='\033[1;35m'
C_CYAN='\033[1;36m'
C_NC='\033[0m'

TOPICS_EXPECTED="['/krock2/jointMeasuredData',
                   '/krock2/jointCommand',
                   '/krock2/imuRPY',
                   '/krock2/pose',
                   '/krock2/robotState',
                   '/vrpn_client_node/krock_front_girdle/pose',
                   '/pool_track/pt_cnt',
                   '/pool_track/pt_cloud']"

TOPICS_INCLUDE="topic in $TOPICS_EXPECTED"
TOPICS_EXCLUDE="topic not in $TOPICS_EXPECTED"

CAMERA_TOPIC_PREFIX="usb_cam"
# Camera exclude
# CAMERA_TOPICS_EXCLUDE="topic != '/usb_cam/camera_info' and topic != '/usb_cam/image_raw'"
CAMERA_TOPICS_EXCLUDE=$TOPICS_INCLUDE
CAMERA_NAME_EXCLUDE="_wo-camera"
CAMERA_DIR_EXCLUDE="wo-camera"
# Camera include
# CAMERA_TOPICS_INCLUDE="topic == '/usb_cam/camera_info' or topic == '/usb_cam/image_raw'"
CAMERA_TOPICS_INCLUDE=$TOPICS_EXCLUDE
CAMERA_NAME_INCLUDE="_camera"
CAMERA_DIR_INCLUDE="camera"

# Create two directories for include and exclude if they do not exist
[ ! -d "./$CAMERA_DIR_EXCLUDE" ] && mkdir $CAMERA_DIR_EXCLUDE 
[ ! -d "./$CAMERA_DIR_INCLUDE" ] && mkdir $CAMERA_DIR_INCLUDE

# Set space as the delimiter
IFS='.'

for BAG_FILE_NAME in *.bag; do # Whitespace-safe but not recursive.
    echo -e "${C_CYAN} Processing file: ${C_YELLOW} $BAG_FILE_NAME ${C_NC} "

	# check if camera topics are in the bag
	if !( rosbag info "$BAG_FILE_NAME" | grep -q "usb_cam" )
	then
		echo -e "${C_CYAN} [$BAG_FILE_NAME] ${C_YELLOW} No topics with prefix $CAMERA_TOPIC_PREFIX found ${C_NC} "
		echo -e "${C_CYAN} [$BAG_FILE_NAME] ${C_YELLOW} Processing the bag file anyway ${C_NC} "
	fi

	read -a strarr <<< "$BAG_FILE_NAME"
	if [ -e $CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag ] | [ -e $CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag ]
	then
			# Print the processed files found
			echo -e "${C_CYAN} [$BAG_FILE_NAME] ${C_ORANGE} Processed files already exists ${C_NC}"
			[ -e $CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag ] && echo -e "${C_ORANGE} Found: $CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag ${C_NC}"
			[ -e $CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag ] && echo -e "${C_ORANGE} Found: $CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag ${C_NC}"
	else
			# Without Camera feed (wo-camera)
			# Filtering
			echo -e "${C_CYAN} Filtering for CAMERA_TOPICS_EXCLUDE ${C_NC}: $CAMERA_TOPICS_EXCLUDE"
			echo -e "${C_CYAN} Saving to ${C_NC}: $CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag"
			rosbag filter "$BAG_FILE_NAME" "$CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag" "$CAMERA_TOPICS_EXCLUDE"
			# Compress
			echo -e "${C_CYAN} Compressing ${C_NC}: $CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag"
			if rosbag compress "$CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag" ; then
				# Remove backup if compress successful
				echo -e "${C_CYAN} Rosbag Compressed: ${C_NC} Deleting backup"
				rm "$CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.orig.bag"
			else
				echo "${C_RED} Failed: rosbag compress "$CAMERA_DIR_EXCLUDE/${strarr[0]}${CAMERA_NAME_EXCLUDE}.bag" ${C_NC}"
			fi

			# Wit Camera feed (camera)
			# Filtering
			echo -e "${C_CYAN} Filtering for CAMERA_TOPICS_INCLUDE ${C_NC}: $CAMERA_TOPICS_INCLUDE"
			echo -e "${C_CYAN} Saving to ${C_NC}: $CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag"
			rosbag filter "$BAG_FILE_NAME" "$CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag" "$CAMERA_TOPICS_INCLUDE"
			# Compress
			echo -e "${C_CYAN} Compressing ${C_NC}: $CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag"
			if rosbag compress "$CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag" ; then
				# Remove backup if compress successful
				echo -e "${C_CYAN} Rosbag Compressed: ${C_NC} Deleting backup"
				rm "$CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.orig.bag"
			else
				echo "${C_RED} Failed: rosbag compress "$CAMERA_DIR_INCLUDE/${strarr[0]}${CAMERA_NAME_INCLUDE}.bag" ${C_NC}"
			fi
	fi
done


# Relative direcotry
# REL_DIR="raw/"
# BAGS=$(ls $REL_DIR*.bag)
# TOPICS=" topic in ['/krock2/jointMeasuredData', '/krock2/jointCommand', '/krock2/imuRPY', '/krock2/pose', '/vrpn_client_node/krock_front_girdle/pose']"

# for FILE in $BAGS
# do
# 	  BASE_NAME=$(basename ${FILE%.bag}) # strip directory and filetype
# 	  BAG_OUTPUT="filtered/${BASE_NAME}-filtered.bag"
# 	  rosbag filter $FILE $BAG_OUTPUT "topic == $TOPICS"
# 	  rosbag compress $BAG_OUTPUT
# done

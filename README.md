# experiment-bags

Selected and filtered bag files stored with Git LFS.

Bag files kept in this directory are to be filtered, compressed, and stored with [Git LFS](https://git-lfs.github.com/).
Unfiltered bag files are kept long-term in `biorobnfs.epfl.ch,share=projects/NCCR/bagfiles`.

## Git Large File Storage (LFS)

Check if 'git-lfs' is installed with `dpkg -s git-lfs`. Otherwise, install git-lfs using:
 
    sudo apt-get install git-lfs

Advantages of Git LFS for us are 
- Same access controls and permissions
- Same git workflow
- Storage-efficient versioning of large files (quote below)

> \[With git lfs\] Large files are stored in the cloud, and these files are referenced via pointers in local copies of the repo. When a clone or pull occurs, the appropriate version of the file is downloaded from the remote. Updates to a large file will still create multiple copies of the file, but these copies will be stored on the cloud and won’t have to be downloaded by everyone who clones the repo. [- Forestry.io](https://forestry.io/blog/versioning-large-files-with-git-lfs/)
 
## Organization and documentation

Each project or set of experiments should be stored in its own subdirectory (e.g. `idsia-optitrack/`) along with documentation of the experiment(s). Ultimately we are trying to make the data parsing reproducible. Nice read on tips/philosophy [here](https://kbroman.org/steps2rr/).

## Camera data filtering 

`filtering_bagfiles.sh` added to help filter and separate topics for camera data.   

Aim: The script reads the bag files in the **current diretory** and generates 2 new bags as shown below
- original data, w/o camera data
- original data, only camera data

These files are created under separate subfolders under the **current diretory** 

Run: run the script from the directory that has a ros bag 

	$ cd path/to/bagfiles 
	$ source ./path/to/script/filtering_bagfiles.sh
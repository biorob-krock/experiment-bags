# System Properties of Krock in Reality and Simulation

## Krock in Webots

### Coulomb Friction

One of the most influential parameters on the outcome of the experiments is friction. For that, there are two modified `coulombFriction` properties in the `Worldinfo` node of `webots`. Follow the proceeding steps to find these variables:

- Open the first (`Worldinfo`) node of the `webots` tree

- Select `contactProperties` and choose the first `ContactProperties` node

- `material1` is defined as "foot". Also, the variable `bounce` is set to ero. Finally, `coulombFriction` is set to `0.5`

- For the second `ContactProperties` node (which I suppose corresponds to the friction of the body on ground) `coulombFriction` is set to `0.25`

### Weight

To measure the net weight of the robot in the simulation, we sould add all the `physics` node present in the robot. The difference in speed of the `Krock` can come from weight difference of the simulation and real robot.

Based on one of the prior studies by Francisco, the net weight of `Krock` in simulation is [**4.8Kg**](https://gitlab.com/biorob-krock/visualization/-/blob/master/doc/francisco-visualization.org). This link is the reference fot the calculation.

## Krock in Reality

### Coulomb Friction

To be determined

### Weight

According to the point load scales that we have in the lab, the weight of krock is **55 N** which maps to **5.6 Kg** that means on a 25 degrees ramp, we have about **5 N** difference in the force that pulls the robot down. 
# Experimental Procedure

OptiTrack experiments to verify IDSIA traversability calculations.

Measurements of the obstacles are found in `.odp` images, photographs in `.jpg`,
and 2D pixel images corresponding to the height map of the obstacle in `.png` files. 

## Description of Trials

**STARTING X (FORWARD DIRECTION) POSITION OF THE ROBOT IN EXPERIMENTS**: This parameter was measured based on the distance of the nose of `Krock` from the starting point of the obstacle (further details can be found in `.odp` or `setup#_description_specs.png` in specs folder). Very important note is that this parameter varies from one experiment to another due to space limitations of `OptiTrack` arena. 

We refer to **STARTING X DISTANCE** as the X coordinate of the `OptiTrack` marker centroid from our set origin. For example for setup1 according to the following figure we have:

[![M](img/experiment_specs/setup1_plateau_specs.png)](img/frame.png)

X_START = OBSTACLE_OFFSET - NOSE_TO_OBSTACLE - NOSE_TO_MARKER = 0.8m

## Plateau (Setup 1)

Testing traversability using a "low gait" as it was easily traversable and we didn't have the high gait yet.

Robot traversed over a small [1m^2], square patch that was about 6cm height. 

**NOTE**: The picture for setup1 is not exactly the one that we had in our experiments. However, the height of the obstacle is same and there is no loss of information as we have the heightmap as `setup1_plateau.png` in this folder (there is no picture but can be taken easily if necessary)


#### **STARTING X DISTANCE: `0.8m`**

  X_START = 1.715 - 0.85 - 0.065 = 0.8m

  **NOTE**: This parameter was not carefully controlled for these experiments.


#### **Tested Gaits**

- low

## Steps + Ramp (Setup 2)

Conducted the experiments with both low and high gaits which are mentioned in the name of the bag files.

Obstacle was several steps then a small ramp leading down.

#### **STARTING X DISTANCE: `0.65m`**

  X_START = 1.715 - 1 - 0.065 = 0.65m

#### **Tested Gaits**

- low: 4 trials
- high: 4 trials

## 3 Bars (Setup3)

Three bars of height were arranged in a row, with heights of `12cm` and `10cm`

#### **STARTING X DISTANCE: `0.935m`**

  X_START = 1.5 - 0.5 - 0.065 = 0.935m

#### **Tested Gaits**

- low: 6 trials
- high: 6 trials

## Ramp 25 (Setup4)

A 25 degree ramp and a box for the `krock` to rest on top of it. This ramp is on the threshold of the robot's traversability

#### **STARTING X DISTANCE: `0.715m`**

  X_START = 1.38 - 0.6 - 0.065 = 0.715m

#### **Tested Gaits**
- low: 6 trials
- high: 4 trials

## StepRamp (Setup5)

This setup is suited for combined gaits (based on prior experimental results). The step is only traversable with the high gait, and the ramp is best to be traversed via low gait. No experiments were carried so far.

## Steps and Stairs (Setup6)

A setup composed of steps with different heights to distinguish between the capabilities of different gaits

#### **STARTING X DISTANCE: `0.535m`**

X_START = 1 - 0.4 - 0.065 = 0.535m


#### **Tested Gaits**

- low: 3
- high: 3
- mixed: 3

#### **Locations for changing the gaits**

We start the mixed experiments with high gait. Then, we switch to low gait at **1.35m**, switch to high gait at **2.08m** and finally switch to low at **2.93m.**. These numbers should be used in the simulation since it's meant to be changed automatically based on the GPS information from `Webots`.

## Reference frames

In the following, we have all the required offsets for both `Webots` and `OptiTrack`:

[![N](img/frame.png)](img/frame.png)

## Change of the framework translation

As of **21.10.2020**, there has been a change in the calibration of the `OptiTrack` origin (just the translation). For that, refer to the following figure:

[![Q](img/frame_2.png)](img/frame_2.png)

### 3D View

To have a 3D intuition of the difference between the involving frameworks, refer to the following figure:

[![H](img/3d_frames.png)](img/3d_frames.png)

### Transformation Matrices

The most important transformation is from `OptiTrack` framework to `Webots/Experiment` framework. For that, refer to the following rotation matrix.

[![B](img/R_mat.png)](https://www.andre-gaschler.com/rotationconverter/)

**Translation**: For the translation vector, we have two of them since as of **21.10.2020** we re-calibrated the system. Refer to the following translation matrices for the proper transformation from `OptiTrack` to `Weobts/Experiments` origin.

```math
T1 = [t_x t_y t_z]^T = [-1.32, 0, -1.33]^T
T2 = [t_x t_y t_z]^T = [-0.46, 0, -1.48]^T
```
## Heightmap script

To generate the heightmaps for setups, we used
[`generate_heightmap.py`](https://gitlab.com/biorob-krock/experiment-parsing/-/blob/master/generate_heightmap.py). So far, we have 4 functions that generate 4 experimental setups as following:

```py
p_height = 0.06 # Plateau height in meters
setup1_hmap = generatePlateau(30, 60, p_height,pixDen)
imageio.imwrite('setup1.png',setup1_hmap.astype(np.uint16))

setup2_hmap = generate_2nd_exp(pixDen)
imageio.imwrite('setup2.png',setup2_hmap.astype(np.uint16))

setup3_hmap = generate_3rd_exp(pixDen)
imageio.imwrite('setup3.png',setup3_hmap.astype(np.uint16))

setup4_hmap = generate_4th_exp(pixDen)
imageio.imwrite('setup4.png',setup4_hmap.astype(np.uint16))
```

Each of the code blocks can be commented to save only the required heightmaps.